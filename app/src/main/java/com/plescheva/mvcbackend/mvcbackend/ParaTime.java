package com.plescheva.mvcbackend.mvcbackend;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class ParaTime {
    private int paraTimeId;
    private String paraTimeNameSokr;
    private String paraTimeNameFull;
    private String paraTimeTo;

    ParaTime(JSONObject jsonObject) throws JSONException {
        paraTimeFrom = jsonObject.getString("paraTimeId");
        paraTimeNameSokr = jsonObject.getString("paraTimeNameSokr");
        paraTimeNameFull = jsonObject.getString("paraTimeNameFull");
        paraTimeTo = jsonObject.getString("paraTimeTo");
        paraTimeFrom = jsonObject.getString("paraTimeFrom");
    }

    public int getParaTimeId() {
        return paraTimeId;
    }

    public void setParaTimeId(int paraTimeId) {
        this.paraTimeId = paraTimeId;
    }

    public String getParaTimeNameSokr() {
        return paraTimeNameSokr;
    }

    public void setParaTimeNameSokr(String paraTimeNameSokr) {
        this.paraTimeNameSokr = paraTimeNameSokr;
    }

    public String getParaTimeNameFull() {
        return paraTimeNameFull;
    }

    public void setParaTimeNameFull(String paraTimeNameFull) {
        this.paraTimeNameFull = paraTimeNameFull;
    }

    public String getParaTimeTo() {
        return paraTimeTo;
    }

    public void setParaTimeTo(String paraTimeTo) {
        this.paraTimeTo = paraTimeTo;
    }

    public String getParaTimeFrom() {
        return paraTimeFrom;
    }

    public void setParaTimeFrom(String paraTimeFrom) {
        this.paraTimeFrom = paraTimeFrom;
    }

    String paraTimeFrom;

}
