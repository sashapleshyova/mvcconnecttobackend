package com.plescheva.mvcbackend.mvcbackend;

import java.util.List;

public interface VolleyCallback {
    void onSuccess(List<ParaTime> paraTimes);
}
