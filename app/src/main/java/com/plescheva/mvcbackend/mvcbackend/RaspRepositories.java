package com.plescheva.mvcbackend.mvcbackend;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


public class RaspRepositories {

    private List<ParaTime> paraTimes = new ArrayList<>();
    VolleyCallback callback = null;
    Context context;

    public RaspRepositories(Context context, VolleyCallback callback) {
        this.callback = callback;
        this.context = context;
    }

    public void getParaTimes()  {
        String url = "http://box1.binarus.ru/TimeTableBD/api/ParaTime.json?token=5EiXMVQlS45g6IHi1FlC8oBK644kkxccy";
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.GET, url,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("paraTimes");
                    for (int i=0; i<jsonArray.length(); i++){
                        paraTimes.add(new ParaTime(jsonArray.getJSONObject(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                callback.onSuccess(paraTimes);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("err ", String.valueOf(error));
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(strReq);

    }
}
