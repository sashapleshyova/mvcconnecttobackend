package com.plescheva.mvcbackend.mvcbackend;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class MainActivity extends AppCompatActivity{

    VolleyCallback callback = null;
    RaspRepositories repositories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        call();
        repositories = new RaspRepositories(MainActivity.this, callback);
        repositories.getParaTimes();


    }

    private void call(){
        callback = new VolleyCallback() {
            @Override
            public void onSuccess(List<ParaTime> paraTimes) {
                Log.i("main ", String.valueOf(paraTimes.size()));
            }
        };
    }

}
